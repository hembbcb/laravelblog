<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if(env('APP_ENV')=='local'){

            $this->call(UsersTableSeeder::class);
            $this->call(PostsTableSeeder::class);
            $this->call(TagsTableSeeder::class);
            $this->call(CategoriesTableSedder::class);
            
        }
        else
        {

            $this->call(UsersTableSeeder::class);
            $this->call(CategoriesTableSedder::class);
        }
  
    
    }
}
