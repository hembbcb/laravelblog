@extends('layouts.main')

@section('content')


    <div class="container">
        <div class="row">
            <div class="col-md-8">

            @if (! $posts->count())

                <div class="alert alert-info"  padding= '10'>
                          <h1>No Post Found</p>

                </div>

            @elseif($term = request('term'))
            <div class="alert alert-info"  padding= '10'>
                          <p>Search Results for: <strong>{{ $term }}</strong></p>

                </div>

            @endif
             

                  @foreach($posts as $post)
                 
                <article class="post-item">

                @if ($post->image_url)

                    <div class="post-item-image">
                    <a href="{{route('blog.show', $post->id)}}">
                            <img src="{{$post->image_url}}" alt="">
                        </a>
                    </div>

                @endif

                    <div class="post-item-body">
                        <div class="padding-10">
                        <h2><a href="{{route('blog.show', $post->id)}}">{{$post->title}}</a></h2>
                            {!! $post->excerpt_html !!}
                        </div>

                        <div class="post-meta padding-10 clearfix">
                            <div class="pull-left">
                                <ul class="post-meta-group">
                                    <li><i class="fa fa-user"></i><a>{{$post->author->name}}</a></li>
                                    <li><i class="fa fa-clock-o"></i><time>{{$post->date}}</time></li>
                                    <li><i class="fa fa-folder"></i><a href="{{route('category', $post->category->id)}}">{{ $post->category->title}}</a></li>
                                    <li><i class="fa fa-tag"></i>{!!$post-> tags_html !!}</li>
                                </ul>
                            </div>
                            <div class="pull-right">
                                <a href="{{route('blog.show', $post->id)}}">Continue Reading &raquo;</a>
                            </div>
                        </div>
                    </div>
                </article>


                @endforeach


                <nav>
                        {{$posts->appends(request()->only(['term']))->links() }}
                </nav>
            </div>
            @include('layouts.sidebar')

            </div>
    </div>

@endsection

