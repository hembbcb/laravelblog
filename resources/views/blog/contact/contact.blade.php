@extends('layouts.main')

@section('content')

        <div class="container">
            <div class="content">
                <div class="col-md-12" style="text-align:center; font-size:medium;font-family:sans-serif;">
                    
                <h2 style="text-align: center;font-weight:bolder; font-size:large; padding: 40px 0;">  CONTACT DETIALS</h2>

                
                <p><strong> Website: </strong><a href="https://geospatialbhutan.herokuapp.com">https://geospatialbhutan.herokuapp.com </a></p>
                <p><strong>Email: </strong> geospatialbhutan@gmail.com</p>
                <p><strong>Address: </strong> Thimphu:Bhutan</p>

                </div>
            </div>
        </div>

@endsection