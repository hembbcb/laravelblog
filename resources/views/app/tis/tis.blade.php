@extends('layouts.main')

@section('content')

        <div class="container">
            <div class="content">
                <div class="col-md-12" style="text-align:justify; font-size:medium;font-family:sans-serif;">
                    
                <h2 style="text-align: center;font-weight:bolder; font-size:large; padding: 40px 0;"> ABOUT TOURISM INFORMATION SYSTEM</h2>

                <p>The tourism information system serves as tourism information centres which offers well organised, customer friendly and high quality tourism information to both internal tourists and foreign visitors.</p>
                
                <p><strong> Website: </strong><a href="#">Druk Tourism Information System</a></p>

                </div>
            </div>
        </div>

@endsection