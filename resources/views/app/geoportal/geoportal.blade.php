@extends('layouts.main')

@section('content')

        <div class="container">
            <div class="content">
                <div class="col-md-12" style="text-align:justify; font-size:medium;font-family:sans-serif;">
                    
                <h2 style="text-align: center;font-weight:bolder; font-size:large; padding: 40px 0;"> ABOUT DRUK GEO-PORTAL</h2>

                <p>Geoportal are important for effective use of geographic information systems (GIS) and a key element of Spatial Data Infrastructure.
                </br>
                Druk geoportal is a type of web portal used to view geographic information and associated geographic services via the Internet.
                </br>
                This portal allows users to enhance the awareness about geospatial, and improve access to geospatial information and different applications.</p>
                </br>
                <p><strong> Website: </strong><a href="https://drukgeoportal.herokuapp.com/">Druk Geoportal</a></p>

                </div>
            </div>
        </div>

@endsection